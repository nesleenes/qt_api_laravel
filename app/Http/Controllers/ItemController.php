<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function get_item($id) {
  


        $qt = DB::select(DB::raw("
        SELECT * FROM item LEFT JOIN quotation as quotation ON quotation.quotation_id = item.item_quotation_id WHERE quotation.quotation_id = ".$id."
        "));

        foreach ($qt as $row) {

            $original_date = $row->issue_date;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issueDate = $new_date;

            $original_date2 = $row->expire_date;
            // Creating timestamp from given date
            $timestamp2 = strtotime($original_date2);
            // Creating new date format from that timestamp
            $new_date2 = date("d-m-Y", $timestamp2);
            $row->expire_date = $new_date2;


            $original_date3 = $row->delivery_date;
            // Creating timestamp from given date
            $timestamp3= strtotime($original_date3);
            // Creating new date format from that timestamp
            $new_date3 = date("d-m-Y", $timestamp3);
            $row->delivery_date = $new_date3;

        }

        echo json_encode($qt);
   }
}
