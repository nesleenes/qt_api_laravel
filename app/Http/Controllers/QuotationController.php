<?php

namespace App\Http\Controllers;
use Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    //
    public function index($id) {
    

        $qt = DB::select(DB::raw("
            
            SELECT * FROM quotation as qt
            LEFT JOIN customer as ct ON qt.quotation_customer_id = ct.customer_id WHERE qt.quotation_id = '".$id."'
        
        "));

        foreach ($qt as $row) {

            $original_date = $row->issue_date;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issue_date = $new_date;


            $original_date2 = $row->expire_date;
            // Creating timestamp from given date
            $timestamp2 = strtotime($original_date2);
            // Creating new date format from that timestamp
            $new_date2 = date("d-m-Y", $timestamp2);
            $row->expire_date = $new_date2;


            $original_date3 = $row->delivery_date;
            // Creating timestamp from given date
            $timestamp3= strtotime($original_date3);
            // Creating new date format from that timestamp
            $new_date3 = date("d-m-Y", $timestamp3);
            $row->delivery_date = $new_date3;

        }
        if (!empty($qt)){
            echo json_encode($qt[0]);
        } else {
            return response($qt);
        }
        
   }


   public function get_all() {
    

    $qt = DB::select(DB::raw("
        
    SELECT * FROM quotation as qt
            LEFT JOIN customer as ct ON qt.quotation_customer_id = ct.customer_id
    
    "));
    foreach ($qt as $row) {

        $original_date = $row->issue_date;
        // Creating timestamp from given date
        $timestamp = strtotime($original_date);
        // Creating new date format from that timestamp
        $new_date = date("d-m-Y", $timestamp);
        $row->issue_date = $new_date;
    }
    // echo  json_encode($qt) ;
    return response($qt);
}



    public function get_quotation() {

    }

    public function insert_quotation(Request $request) {
    
        $data = json_decode($request->getContent(),true);

        $customer_id = DB::table('customer')->insertGetId([
            'name_customer' => $data['name_customer'],
            'email' => $data['email'],
            'phone_no_customer' => $data['phone_no_customer'],
            'district' => $data['district'],
            'province' => $data['province'],
            'amphoe' => $data['amphoe'],
            'zipcode' => $data['zipcode'],
            'address' => $data['address'] 
        ]);
        
        $quotation_id = DB::table('quotation')->insertGetId([
            
            'issue_date' => $data['issue_date'],
            'expire_date' => $data['expire_date'],
            'delivery_date' => $data['delivery_date'],
            'validity' => $data['validity'],
            'saleman_name' => $data['saleman_name'],
            'phone_no' => $data['phone_no'],
            'credit' => $data['credit'],
            'vat_type' => $data['vat_type'],
            'vat_amt' => $data['vat_amt'],
            'discount_type' => $data['discount_type'],
            'discount_amt' => $data['discount_amt'],
            'sub_total' => $data['sub_total'],
            'total' => $data['total'],
            'note' => $data['note'],
            'quotation_customer_id' => $customer_id,
        ]);

        foreach($data['itemList'] as $row ) {
            DB::table('item')->insert([
                'name' => $row['name'],
                'amount' => $row['amount'],
                'price' => $row['price'],
                'discount' => $row['discount'],
                'sum' => $row['sum'],
                'item_quotation_id' => $quotation_id,
            ]);
        }

        $response['id'] = $quotation_id;
        return response($response);
    }

    function quotation_detail($id) {
        
        $qt = DB::select(DB::raw("
            
            
            SELECT * FROM quotation as qt
            LEFT JOIN customer as ct ON qt.customer_id = ct.customer_id 
            WHERE qt.customer_id = ".$id."
        
        "));

        foreach ($qt as $row) {

            $original_date = $row->expire_date;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->expire_date = $new_date;

            $item = DB::select(DB::raw("
            
                SELECT * FROM item as it
                WHERE it.item_quotation_id = ".$id."
            
            "));

            $row->item = $item; 

        }
        
       

        echo json_encode($qt);
    }

    function get_customer($id) {
        
        $qt = DB::select(DB::raw("
            
            SELECT * FROM customer where customer_id=".$id."
        
        "));

      

        echo json_encode($qt);
    }
    function destroy_quotation($id){
        $qt = DB::select(DB::raw("
            
            
        DELETE FROM `quotation` WHERE `quotation`.`quotation_id`  = ".$id."
    
    "));
    }
    function update_quo($id){
        $qt = DB::select(DB::raw("
        UPDATE `quotation` SET `expire_date` = '3' ,
        'delivery_date' = '', 
        'saleman_name' ='',
        'phone_no'='',
        'credit'='', 
        'vat_amt'='', 
        'discount_type'='',
        'discount_amt'='', 
        'sub_total' = '' ,
        'note'='' 
        WHERE `quotation`.`quotation_id` = 17;
        "));
    }
}
