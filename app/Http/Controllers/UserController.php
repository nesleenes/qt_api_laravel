<?php

namespace App\Http\Controllers;
use Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// SELECT * FROM quotation LEFT JOIN user ON quotation.id = user.id_user WHERE user.id_user=id
 // SELECT * FROM user LEFT JOIN quotation ON quotation.id = user.id_user WHERE user.id_user=id
class UserController extends Controller
{
    // public function insert_user(Request $request)
    // {
    //     $data = json_decode($request->getContent(),true);
    //     $user_id = DB::table('customer')->insertGetId([
    //         'name' => $data['name'],
    //         'lastname' => $data['lastname'],
    //         'username' => $data['username'],
    //         'password' => $data['password'],
    //         'email' => $data['email'],
    //         'position' => $data['position'],
    //         'phoneNumber' => $data['phoneNumber'],
    //     ]);
    //     echo($data);
    //     return response($user_id);
    // }
    public function insert_user(Request $request) {
        $data = json_decode($request->getContent(),true);
        $id_user=2;
        $name=$data['name'];
        $lastname=$data['lastname'];
        $position=$data['position'];
        $email= $data['email'];
        $status='open';
        $username=$data['username'];
        $password=$data['password'];
        
        $qt = DB::insert(DB::raw("
        INSERT INTO user (id_user,name, lastname, position, email, status, username, password)
        VALUES ( id_user,'$name', '$lastname','$position', '$email', '$status', '$username', '$password')
        "));
        // foreach ($qt as $row) {

        //     $original_date = $row->issueDate;
        //     // Creating timestamp from given date
        //     $timestamp = strtotime($original_date);
        //     // Creating new date format from that timestamp
        //     $new_date = date("d-m-Y", $timestamp);
        //     $row->issueDate = $new_date;

        // }

        echo json_encode($qt);
   }
   public function get_AllUser() {
  


    $qt = DB::select(DB::raw("
    SELECT * FROM `user` WHERE 1
    "));

    // foreach ($qt as $row) {

    //     $original_date = $row->issueDate;
    //     // Creating timestamp from given date
    //     $timestamp = strtotime($original_date);
    //     // Creating new date format from that timestamp
    //     $new_date = date("d-m-Y", $timestamp);
    //     $row->issueDate = $new_date;

    // }

    echo json_encode($qt);
}
   public function get_User($id) {
  


    $qt = DB::select(DB::raw("
    SELECT * FROM `user` WHERE `id_user`=".$id."
    "));

    // foreach ($qt as $row) {

    //     $original_date = $row->issueDate;
    //     // Creating timestamp from given date
    //     $timestamp = strtotime($original_date);
    //     // Creating new date format from that timestamp
    //     $new_date = date("d-m-Y", $timestamp);
    //     $row->issueDate = $new_date;

    // }

    echo json_encode($qt);
}

    public function get_quotation_from_UserId() {
  

        $id = 1;

        $qt = DB::select(DB::raw("
        SELECT * FROM quotation LEFT JOIN user ON quotation.id = user.id_user WHERE user.id_user=id
        "));

        foreach ($qt as $row) {

            $original_date = $row->issueDate;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issueDate = $new_date;


            $original_date2 = $row->expire_date;
            // Creating timestamp from given date
            $timestamp2 = strtotime($original_date2);
            // Creating new date format from that timestamp
            $new_date2 = date("d-m-Y", $timestamp2);
            $row->expire_date = $new_date2;

        }

        echo json_encode($qt);
   }

}