<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuotationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/quotation/{id}', 'App\Http\Controllers\QuotationController@index');
Route::get('/get_quotation', 'App\Http\Controllers\QuotationController@get_all');
Route::get('/detail/{id}', 'App\Http\Controllers\QuotationController@quotation_detail');
Route::post('/test', 'App\Http\Controllers\QuotationController@index2');
Route::DELETE('/delete_quo', 'App\Http\Controllers\QuotationController@destroy_quotation');


Route::get('/get_quotation_from_user', 'App\Http\Controllers\UserController@get_quotation_from_UserId');

Route::get('/user_data/{id}', 'App\Http\Controllers\UserController@get_User');

Route::post('/insert_user', 'App\Http\Controllers\UserController@insert_user');
Route::post('/get_all_user', 'App\Http\Controllers\UserController@get_AllUser');

Route::get('/item/{id}', 'App\Http\Controllers\ItemController@get_item');

Route::get('/customer/{id}', 'App\Http\Controllers\QuotationController@get_customer');

Route::post('/insertQuotation', 'App\Http\Controllers\QuotationController@insert_quotation');